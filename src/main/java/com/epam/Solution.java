package com.epam;

import java.util.Scanner;

public class Solution {
    public static int countPeopleReached(boolean arr[]){
        int counter = 0;
        for(int i = 1;i<arr.length;i++)
            if(arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]){
        for(int i = 1;i<arr.length;i++)
            if(!arr[i])
                return false;
        return true;
    }

    public static void main(String[] args) {
        final int attempts = 100;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter how much people should be at the party: ");
        System.out.println("Number should be bigger than 2");
        int n = sc.nextInt();
        int countTimesFullSpreaded = 0;
        int peopleReached = 0;
        for(int i=0;i<attempts;i++){
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean alreadyHeard = false;
            int nextPerson ;
            int currentPerson = 1;
            while(!alreadyHeard){
                nextPerson = 1 + (int)(Math.random() * (n-1));
                if(nextPerson == currentPerson){
                    while(nextPerson == currentPerson)
                        nextPerson = 1 + (int)(Math.random() * (n-1));
                }
                if(guests[nextPerson])
                {
                    if(rumorSpreaded(guests))
                        countTimesFullSpreaded++;
                    peopleReached = peopleReached + countPeopleReached(guests);
                    alreadyHeard = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }

        System.out.println("Probability that everyone will hear rumor  in "+attempts+" attempts: " +
                (double)countTimesFullSpreaded/attempts*100+"%");
        System.out.println("Average amount of people reached by rumor is: "+peopleReached/attempts);
    }


}
